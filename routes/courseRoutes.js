const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers")
const auth = require("../auth");

// Creating a course
// router.post("/", (req,res) => {

// 	const userData = auth.decode(req.headers.authorization);
// 	isAdmin = userDa
// 	console.log(userData);
// 	CourseController.addCourse(req.body).then((result,userData) => {
// 	res.send(result,userData.isAdmin);
// })});

//auth.verify para maverify ung request
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})
// router.post("/", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	isAdmin = userData.isAdmin;

// 	console.log(isAdmin);

// 	if (isAdmin) {

// 	courseController.addCourse(req.body).then(result => res.send(result));
// }	else{
	
// 	res.send(false);
// }


// });



module.exports = router;