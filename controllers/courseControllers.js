const Course = require("../models/Course");

//Create a new course
/*
Steps: (pwede rin na magvalidation muna bago magcreate)
1. Create a new Course object
2. Save to the database
3. Error handling

*/

module.exports.addCourse = (reqBody) =>{

console.log(reqBody);
                                 
	//Create a new object
	let newCourse = new Course ({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object
	return newCourse.save().then((course,error) => {
		//Course Creation failed
		if(error) {
			return false;
		}else{
			//if course creation is successful
			return true;
		}
	})
} 
//Sir Orlando answer
// module.exports.addCourse = (reqBody) => {

// 	console.log(reqBody);

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }